# 
# Do NOT Edit the Auto-generated Part!
# Generated by: spectacle version 0.32
# 

Name:       openrepos-nocache

# >> macros
# << macros
%define upstream_name nocache

Summary:    minimize the effect an application has on the Linux file system cache
Version:    1.1
Release:    2
Group:      Applications/System
License:    BSD-2
URL:        https://github.com/Feh/nocache
Source0:    nocache-1.1.tar.gz
Source100:  openrepos-nocache.yaml

%description
%{summary}.

%prep
%setup -q -n %{upstream_name}-%{version}

# >> setup
# << setup

%build
# >> build pre
make %{?_smp_mflags} PREFIX=%{_prefix} LIBDIR=/libexec/%{name} all nocache.global
# << build pre



# >> build post
# << build post

%install
rm -rf %{buildroot}
# >> install pre
# << install pre

# >> install post
%__install -Dpm 0644 nocache.so %{buildroot}/%{_libexecdir}/%{name}/nocache.so
%__install -Dpm 0755 nocache.global  %{buildroot}%{_bindir}/nocache
%__install -Dpm 0755 cachedel %{buildroot}%{_bindir}/cachedel
%__install -Dpm 0755 cachestats  %{buildroot}%{_bindir}/cachestats
# << install post

%files
%defattr(-,root,root,-)
%{_libexecdir}/%{name}/nocache.so
%{_bindir}/*
# >> files
# << files
